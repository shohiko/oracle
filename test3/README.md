# 班级：20级软件工程2班

# 学号：202010414215

# 姓名：邵文彦

# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

1.使用sql-developer创建orders表和order_details表。orders表按订单日期order_date设置4个范围分区

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 ,CONSTRAINT customer_name_idx UNIQUE (customer_name)
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
```

![1](1.png) 

- 创建order_details表的语句如下：order_details表设置引用分区。

```、sql
CREATE TABLE order_details
(
 id NUMBER(10, 0) NOT NULL,  
 order_id NUMBER(10, 0) NOT NULL,
 product_id VARCHAR2(40 BYTE) NOT NULL,  
 product_num NUMBER(8, 2) NOT NULL,  
 product_price NUMBER(8, 2) NOT NULL,  
 CONSTRAINT order_details_fk1  
 FOREIGN KEY (order_id) REFERENCES orders(order_id)
)
TABLESPACE USERS
PARTITION BY REFERENCE (order_details_fk1);
```

![2](2.png) 

2.创建两个序列分别用于orders表的order_id和order_details表的id,插入数据的时候，不需要手工设置这两个ID值。

```sql
CREATE SEQUENCE seque_order_id
START WITH 1 
INCREMENT BY 1
MAXVALUE 9999999999
CYCLE;

CREATE SEQUENCE seque_order_details_id 
START WITH 1
INCREMENT BY 1
MAXVALUE 9999999999
CYCLE;
```

![3](3.png) 

3.插入40万行orders表数据和200万行order_details表数据。

（1）不分区插入数据

```sql
DECLARE
    i INTEGER;
    y INTEGER;
    m INTEGER;
    d INTEGER;
    str VARCHAR2(100);
BEGIN
    i:=0;
    y:=2020;
    m:=12;
    d:=12;
    str:=y||'-'||m||'-'||d;
    WHILE i<400000 LOOP
        i:=i+1;
        INSERT into orders(order_id,customer_name ,customer_tel , order_date,employee_id )
            values(seque_order_id.NEXTVAL, 'cus'||i,'12432',to_date(str, 'yyyy-MM-dd'),321);
        insert into order_details
            values(seque_order_details_id.NEXTVAL ,seque_order_id.CURRVAL, i*5, 1, 1);
    end loop;
    commit;
end;
```

 ![4](4.png)

执行联合查询语句并分析执行计划。

```sql
SELECT * FROM orders o 
JOIN order_details d ON o.order_id=d.order_id;
```

![5](5.png) 



## 实验注意事项

- 完成时间：2023-05-08，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test3目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
