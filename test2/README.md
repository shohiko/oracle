

# 班级：20级软件工程2班

# 学号：202010414215

# 姓名：邵文彦

# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色role2，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有role2的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户swy2，给用户分配表空间，设置限额为50M，授予role2角色。
- 最后测试：用新用户swy2连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 第1步：以system登录到pdborcl，创建角色role2和用户swy2，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE role2;
GRANT connect,resource,CREATE VIEW TO role2;
CREATE USER swy2 IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER swy2 default TABLESPACE "USERS";
ALTER USER swy2 QUOTA 50M ON users;
GRANT role2 TO swy2;
```

![](1.png)

- 第2步：新用户swy2连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus swy2/123@pdborcl
SQL> show user;
USER is "swy2"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
```

![](2.png) 

- 第3步：用户hr连接到pdborcl，查询swy2授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
SQL> SELECT * FROM sale.customers_view;
```

![](3.png) 

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。

  ![](4.png) 

- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
```

![](5.png) 

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色role2和用户swy2。
> 新用户swy2使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- autoextensible是显示表空间中的数据文件是否自动增加。

- MAX_MB是指数据文件的最大容量。

  ![](6.png)

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
```

![](7.png)	


## 实验注意事项

- 完成时间：2023-04-25，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test2目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
