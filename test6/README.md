﻿# 班级：20级软件工程2班

# 学号：202010414215

# 姓名：邵文彦

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

## 一、表及表空间设计方案：

### 表设计方案：设计4张表

#### 商品表（goods）：

```sql
CREATE TABLE goods (
 goods_id INT NOT NULL PRIMARY KEY,
 goods_name VARCHAR2(20) NOT NULL UNIQUE,
 price VARCHAR2(10) NOT NULL,
 quantity NUMBER NOT NULL,
 manufacture_date VARCHAR2(10) NOT NULL,
 irrelevant_column VARCHAR2(50)
); 
```

#### 管理员表（management）：

```sql
CREATE TABLE management (
 manager_id INT NOT NULL PRIMARY KEY,
 manager_name VARCHAR2(20) NOT NULL,
 manager_tel VARCHAR2(11) NOT NULL,
 manager_sex VARCHAR2(10) NOT NULL CHECK(manager_sex IN ('male', 'female')),
 goods_id INT,
 irrelevant_column VARCHAR2(50),
 FOREIGN KEY (goods_id) REFERENCES goods (goods_id)
); 
```

#### 商品入库表（instorage）：

```sql
CREATE TABLE instorage (
 in_id INT NOT NULL PRIMARY KEY,
 goods_name VARCHAR2(20) NOT NULL,
 in_quantity NUMBER NOT NULL,
 in_time VARCHAR2(10),
 goods_id INT,
 manager_id INT,
 irrelevant_column VARCHAR2(50),
 FOREIGN KEY (goods_id) REFERENCES goods (goods_id),
 FOREIGN KEY (manager_id) REFERENCES management (manager_id)
); 
```

#### 商品出售表（sale）：

```sql
CREATE TABLE sale (
 sale_id INT NOT NULL PRIMARY KEY,
 goods_name VARCHAR2(20) NOT NULL,
 sale_time VARCHAR2(10),
 sale_quantity NUMBER NOT NULL,
 goods_id INT,
 manager_id INT,
 irrelevant_column VARCHAR2(50),
 FOREIGN KEY (goods_id) REFERENCES goods (goods_id),
 FOREIGN KEY (manager_id) REFERENCES management (manager_id)
);
```

![1](1.png) 

![2](2.png) 

### 表空间设计方案：创建2个表空间

#### 表空间tablespace1：

```sql
CREATE TABLESPACE tablespace1 DATAFILE 'tablespace1.dbf' SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;
```

#### 表空间tablespace2：

```sql
CREATE TABLESPACE tablespace2 DATAFILE 'tablespace2.dbf' SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;
```

![3](3.png) 

### 插入10万条模拟数据：

![4](4.png) 

![5](5.png) 

![6](6.png) 

![7](7.png) 

![8](8.png) 

![9](9.png) 

## 二、设计权限及用户分配方案：

### (1).创建role角色并分配给role角色4张表的SELECT、INSERT、UPDATE和DELETE权限：

```sql
CREATE ROLE role;
```

![10](10.png) 

![11](11.png) 

### (2).创建2个用户user1和user2并将role角色授予2个用户:

![12](12.png) 

## 三、在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑:

### (1)存储过程：更新商品价格：

```sql
CREATE OR REPLACE PROCEDURE update_price (
  p_goods_id IN goods.goods_id%TYPE,
  p_price IN goods.price%TYPE
) IS
  BEGIN
    UPDATE goods
    SET price = p_price
    WHERE goods_id = p_goods_id;
  END;
/
```

![13](13.png) 

```sql
DECLARE
  v_goods_id goods.goods_id%TYPE := 2023; 
  v_price goods.price%TYPE := '100'; 
BEGIN
  update_price(v_goods_id, v_price);
  DBMS_OUTPUT.PUT_LINE('商品价格已更新。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('更新失败: ' || SQLERRM);
END;
```

![14](14.png) 

![15](15.png) 

### (2)函数：获取某商品的出库数量：

```sql
CREATE OR REPLACE FUNCTION get_sale_quantity(
    p_goods_id IN goods.goods_id%TYPE
  ) RETURN NUMBER IS
    v_quantity NUMBER;
  BEGIN
    SELECT SUM(sale_quantity) INTO v_quantity
    FROM sale
    WHERE goods_id = p_goods_id;
    RETURN NVL(v_quantity, 0);
  END;
```

![16](16.png) 

#### 测试：

![17](17.png) 

## 四、设计一套数据库的备份方案:

创建备份表:

```sql
CREATE TABLE backups_goods AS SELECT * FROM goods;
CREATE TABLE backups_management AS SELECT * FROM management;
CREATE TABLE backups_instorage AS SELECT * FROM instorage;
CREATE TABLE backups_sale AS SELECT * FROM sale;
```

![18](18.png) 

## 五、实验总结:

表及表空间设计： 在设计商品销售系统的数据库结构时，我根据需求设计了4种表，分别为商品、管理员、商品入库和商品出售表。通过合理的表设计，我能够有效地存储和组织系统所需的数据，并确保数据的完整性和一致性。此外，我还合理地分配了表空间，使得数据库的性能得到了优化。

设计权限及用户分配： 为了保证系统的安全性和数据的保密性，我进行了权限的设计和用户的分配。通过使用Oracle的权限管理功能，我为不同的用户分配了不同的权限，这样做可以有效地保护数据的安全，并确保只有经过授权的用户才能对系统进行操作。

使用PL/SQL设计存储过程和函数： 为了实现复杂的业务逻辑，我使用了PL/SQL语言设计了一些存储过程和函数。这些存储过程和函数能够对数据库进行高效的操作，并实现了一些特定的业务需求，比如更新商品价格、获取某商品的出库数量等。通过使用PL/SQL，我能够在数据库层面上完成复杂的计算和逻辑，提高了系统的性能和可维护性。

通过这次实验，我深刻理解了基于Oracle数据库的商品销售系统的设计和开发过程。我学会了如何合理地设计数据库结构、管理用户权限，以及使用PL/SQL语言实现复杂的业务逻辑。这次实验使我对Oracle数据库的应用有了更深入的认识，并提升了我的数据库设计和开发能力。我相信这些经验和知识将对我今后的学习和工作产生积极的影响。
