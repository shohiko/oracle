CREATE TABLE goods (
 goods_id INT NOT NULL PRIMARY KEY,
 goods_name VARCHAR2(20) NOT NULL UNIQUE,
 price VARCHAR2(10) NOT NULL,
 quantity NUMBER NOT NULL,
 manufacture_date VARCHAR2(10) NOT NULL,
 irrelevant_column VARCHAR2(50)
); 
CREATE TABLE management (
 manager_id INT NOT NULL PRIMARY KEY,
 manager_name VARCHAR2(20) NOT NULL,
 manager_tel VARCHAR2(11) NOT NULL,
 manager_sex VARCHAR2(10) NOT NULL CHECK(manager_sex IN ('male', 'female')),
 goods_id INT,
 irrelevant_column VARCHAR2(50),
 FOREIGN KEY (goods_id) REFERENCES goods (goods_id)
); 
CREATE TABLE instorage (
 in_id INT NOT NULL PRIMARY KEY,
 goods_name VARCHAR2(20) NOT NULL,
 in_quantity NUMBER NOT NULL,
 in_time VARCHAR2(10),
 goods_id INT,
 manager_id INT,
 irrelevant_column VARCHAR2(50),
 FOREIGN KEY (goods_id) REFERENCES goods (goods_id),
 FOREIGN KEY (manager_id) REFERENCES management (manager_id)
); 
CREATE TABLE sale (
 sale_id INT NOT NULL PRIMARY KEY,
 goods_name VARCHAR2(20) NOT NULL,
 sale_time VARCHAR2(10),
 sale_quantity NUMBER NOT NULL,
 goods_id INT,
 manager_id INT,
 irrelevant_column VARCHAR2(50),
 FOREIGN KEY (goods_id) REFERENCES goods (goods_id),
 FOREIGN KEY (manager_id) REFERENCES management (manager_id)
);

CREATE TABLESPACE tablespace1 DATAFILE 'tablespace1.dbf' SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;
CREATE TABLESPACE tablespace2 DATAFILE 'tablespace2.dbf' SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;

CREATE ROLE role;

CREATE OR REPLACE PROCEDURE update_price (
  p_goods_id IN goods.goods_id%TYPE,
  p_price IN goods.price%TYPE
) IS
  BEGIN
    UPDATE goods
    SET price = p_price
    WHERE goods_id = p_goods_id;
  END;
/
DECLARE
  v_goods_id goods.goods_id%TYPE := 2023; 
  v_price goods.price%TYPE := '100'; 
BEGIN
  update_price(v_goods_id, v_price);
  DBMS_OUTPUT.PUT_LINE('商品价格已更新。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('更新失败: ' || SQLERRM);
END;
CREATE OR REPLACE FUNCTION get_sale_quantity(
    p_goods_id IN goods.goods_id%TYPE
  ) RETURN NUMBER IS
    v_quantity NUMBER;
  BEGIN
    SELECT SUM(sale_quantity) INTO v_quantity
    FROM sale
    WHERE goods_id = p_goods_id;
    RETURN NVL(v_quantity, 0);
  END;